package elasticsearch

import (
	"context"
	"encoding/json"
	"fmt"
	"reflect"
	"time"
)

type ElasticDocs struct {
	UrlHashed string
	Url       string
	Text      string
	Create_at time.Time
	Update_at time.Time
}

func jsonStruct(doc ElasticDocs) (string, error) {
	var (
		jsonString string
		err        error
	)

	docStruct := &ElasticDocs{
		UrlHashed: doc.UrlHashed,
		Url:       doc.Url,
		Text:      doc.Text,
		Create_at: doc.Create_at,
		Update_at: doc.Update_at,
	}
	fmt.Println("\ndocStruct:", docStruct)
	fmt.Println("docStruct Type:", reflect.TypeOf(docStruct))
	b, err := json.Marshal(docStruct)
	jsonString = string(b)
	if err != nil {
		fmt.Println("json.Marshal ERROR", err)
		return jsonString, err
	}

	return jsonString, nil
}

func main() {
	ctx := context.Background()

	var (
		docMap map[string]interface{}
	)
	fmt.Println("docMap:", docMap)
	fmt.Println("docMap Type:", reflect.TypeOf(docMap))

}
