package main

import (
	"time"

	elastic "gopkg.in/olivere/elastic.v5"
)

// Elasticsearch struct
type Elasticsearch struct {
	UrlHashed string
	Url       string
	Text      string
	Create_at time.Time
	Update_at time.Time
}

// ESClient is elasticsearch client
var ESClient *elastic.Client

// Connect is getting elasticsearch client
func (elasticsearch Elasticsearch) Connect() *elastic.Client {

	cfg := elasticsearch.Config{
		Addresses: []string{
			"http://localhost:9200",
		},
	}
	es, err := elasticsearch.NewClient(cfg)

}
