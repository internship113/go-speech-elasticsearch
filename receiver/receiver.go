package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

type MongoFields struct {
	Url    string
	Speech []string
}
type RespondFields struct {
	Url    string
	Speech string
}

func insertSpeech(url string, text string) {
	clientOption := options.Client().ApplyURI("mongodb://localhost:27017")
	fmt.Print("clientOption type:", reflect.TypeOf(clientOption), "\n")
	client, err := mongo.Connect(context.TODO(), clientOption)
	ctx, _ := context.WithTimeout(context.Background(), 15*time.Second)
	// Access a MongoDB collection through a database
	col := client.Database("ysr-speech").Collection("text")
	fmt.Print("Collection type:", reflect.TypeOf(col), "\n")
	if err != nil {
		fmt.Println("mongo.Connect() ERROR", err)
		os.Exit(1)
	}
	word := strings.Fields(text)
	log.Println(word, len((word)))
	doc := MongoFields{
		Url:    url,
		Speech: word,
	}
	fmt.Println("doc Type:", reflect.TypeOf(doc))

	result, insertErr := col.InsertOne(ctx, doc)

	if insertErr != nil {
		fmt.Println("InsertOne ERROR", insertErr)
		os.Exit(1)
	} else {
		fmt.Println("InsertOne() result type:", reflect.TypeOf(result))
		fmt.Println("InsertOne() API result:", result)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")

	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channemessagel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"output_ysr_queue", // name
		false,              // durable
		false,              // delete when unused
		false,              // exclusive
		false,              // no-wait
		nil,                // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	var forever chan struct{}

	go func() {
		for d := range msgs {
			message := RespondFields{}
			err := json.Unmarshal(d.Body, &message)
			if err != nil {
				log.Panicln("Unmarshal ERROR", reflect.TypeOf(err))
			} else {
				insertSpeech(message.Url, message.Speech)
			}

			// log.Printf("Received a message: %s", d.Body)
			// log.Printf(strconv.Itoa(1))
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
